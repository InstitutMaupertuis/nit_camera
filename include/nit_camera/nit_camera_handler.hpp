#ifndef NIT_CAMERA_HANDLER
#define NIT_CAMERA_HANDLER

#include <iostream>
#include <memory>
#include <mutex>
#include <vector>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>

#include <opencv2/opencv.hpp>
#include <cyusb.h>

#define VR_SetEP6H 0xD0
#define VR_SetEP6L 0xD1
#define NIT_CAMERA_VR_IO_Control 0xD3

class NITCameraHandler
{
public:

  enum PIXEL_CLOCK_FREQUENCY_MHZ
  {
    F12_5, // 12.5 MHz
    F16_666, // 16.666 MHz
    F20, // 20 MHz
    F25, // 25 MHz
    F33_333, // 33.333 MHz
    F40, // 40 MHz
    F50, // 50 MHz
    F66_666, // 66.666 MHz
    F80 // 80 MHz
  };

  enum ANALOG_GAIN // dB = decibels
  {
    DECIBELS_0,
    DECIBELS_4,
    DECIBELS_8,
    DECIBELS_12
  };

  enum DIGITAL_GAIN
  {
    AGC, // Automatic gain control
    G0_25, // Gain = 0.25
    G0_5, // Gain = 0.5
    G0_75, // Gain = 0.75
    G1, // Gain = 1
    G1_25,
    G1_5,
    G1_75,
    G2, // Gain = 2
    G2_25,
    G2_5,
    G2_75,
    G3, // Gain = 3
    G3_25,
    G3_5,
    G3_75,
    G4, // Gain = 4
    G4_25,
    G4_5,
    G4_75,
    G5, // Gain = 5
    G5_25,
    G5_5,
    G5_75,
    G6, // Gain = 6
    G6_25,
    G6_5,
    G6_75,
    G7, // Gain = 7
    G7_25,
    G7_5,
    G7_75,
  };

  enum SHUTTER_MODE
  {
    GLOBAL,
    ROLLING,
    DIFFERENTIAL
  };

  enum TRIGGER_MODE
  {
    NO_EXTERNAL_SIGNAL,
    EXTERNAL_SIGNAL,
    EXTERNAL_SIGNAL_NO_VOLTAGE_ON_TRIGGER_CONNECTOR
  };

  enum FIXED_PATTERN_NOISE_CORRECTION
  {
    FPN_0_025,
    FPN_0_1,
    FPN_0_4,
    FPN_0_5
  };

  struct SensorDefinitionParameters
  {
    std::string sensor_name;
    unsigned sensor_number;
    SHUTTER_MODE shutter_mode;
    unsigned sensor_width;
    unsigned sensor_height;
    unsigned image_width;
    unsigned image_height;
    unsigned first_colon;
    unsigned first_line;
    double sensor_line_time[9];
    TRIGGER_MODE trigger_mode;
    unsigned pixel_depth;
    bool has_bayer_filter; // Is it a color camera?
    cv::ColorConversionCodes bayer_OpenCV_code;
    unsigned capture_timeout_ms;
    PIXEL_CLOCK_FREQUENCY_MHZ pixel_clock_frequency;
    ANALOG_GAIN analog_gain;
    DIGITAL_GAIN digital_gain;
    unsigned digital_offset;
    FIXED_PATTERN_NOISE_CORRECTION fpn_correction;
    unsigned frames_per_second;
  };

  NITCameraHandler(const SensorDefinitionParameters sensor);
  virtual ~NITCameraHandler();

  int sendParametersToCamera(cyusb_handle* handle);

  void captureFrame(const ros::TimerEvent&);

  unsigned getFrameRate();

  cv::Mat getFrame();

  std::shared_ptr<ros::Publisher> publisher_;

private:
  int sendUSBCommand(cyusb_handle* handle,
                     const int n_request,
                     const int data);

  int sendDataToRegister(cyusb_handle* handle,
                         const int reg_addr,
                         const int reg_val);

  void AGCControlProcessing(const std::vector<short> &HistAGC,
                            cv::Mat &frame);

  cyusb_handle *handle_; // FIXME How to properly release this?
  SensorDefinitionParameters sensor_;
  int endpoint_;
  const int packet_length_;
  uchar *buffer_;
  std::vector<short> hist_AGC_;
  const unsigned total_max_; // FIXME What does this constant mean?
  cv::Mat frame_;
  std::mutex capture_lock_;
  std::mutex hist_AGC_lock_;
  std::mutex frame_lock_;

  ros::NodeHandle nh_;
  ros::Timer timer_;
};

NITCameraHandler::SensorDefinitionParameters NITCameraHandlerSensorDefinitionParameters(const std::string sensor_name);

#endif
