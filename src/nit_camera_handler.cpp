#include "nit_camera/nit_camera_handler.hpp"

//#define CHRONO_DEBUG
//#define CHRONO_DEBUG_USB_COMMAND
#ifdef CHRONO_DEBUG
#include <chrono>
#endif

// FIXME Analog gain not change anything

NITCameraHandler::NITCameraHandler(SensorDefinitionParameters sensor) :
        sensor_(sensor),
        packet_length_(sensor_.image_width * sensor_.image_height * 2),
        total_max_(16383)
{
  buffer_ = new uchar[sensor_.image_width * sensor_.image_height * 2]; // Delete!
  hist_AGC_.resize(total_max_ + 1);

  int device_count = cyusb_open();
  if (device_count == 0)
  {
    throw std::runtime_error("No device found! Make sure camera is plugged-in");
  }
  else if (device_count > 1)
    ROS_INFO_STREAM("Multiple device found, using the first in the list");

  for (int i(0); i < device_count; ++i)
  {
    cyusb_handle* handle_temp(cyusb_gethandle(i));
    if (!handle_temp)
    {
      ROS_WARN_STREAM("Device " << i << " did not return a valid handle");
      continue;
    }

    ROS_INFO_STREAM("Camera " << i << std::endl <<
                    "Vendor ID = " << cyusb_getvendor(handle_temp) << std::endl <<
                    "Product = " << cyusb_getproduct(handle_temp) << std::endl <<
                    "Bus number = " << cyusb_get_busnumber(handle_temp) << std::endl <<
                    "Device address = " << cyusb_get_devaddr(handle_temp));
  }

  // Use first device found
  handle_ = cyusb_gethandle(0);
  if (cyusb_kernel_driver_active(handle_, 0) == 1)
  {
    ROS_INFO_STREAM("Kernel driver active: trying to detach driver...");
    if (cyusb_detach_kernel_driver(handle_, 0) != 0)
      throw std::runtime_error("Could not detach kernel driver");
  }

  if (cyusb_claim_interface(handle_, 0) != 0)
    throw std::runtime_error("Could not claim interface. Is there another program using the camera?");

  int rvalue;
  rvalue = sendParametersToCamera(handle_);
  if (rvalue != 0)
    ROS_WARN_STREAM("Error sending parameters to the camera. rvalue = " << rvalue);

  rvalue = sendUSBCommand(handle_, VR_SetEP6H, 0);
  if (rvalue != 0)
    ROS_WARN_STREAM("Error sending USB command. rvalue = " << rvalue);

  if (cyusb_getproduct(handle_) == 4099) // USB 2 case
    endpoint_ = 0x82;
  else if (cyusb_getproduct(handle_) == 241) // USB 3 case
    endpoint_ = 0x83;
  else
    throw std::runtime_error("Product not recognized!");

  // Create timer to fetch image
  timer_ = nh_.createTimer(ros::Rate(40), &NITCameraHandler::captureFrame, this);
}

NITCameraHandler::~NITCameraHandler()
{
  delete buffer_;
}

int NITCameraHandler::sendParametersToCamera(cyusb_handle* handle)
{
  // Lock capture until we have a finished sending camera parameters
  std::lock_guard<std::mutex> guard(capture_lock_);

  int i_val;
  double d_Tlines, d_RefClk;

  // Shutter mode and trigger mode
  i_val = sensor_.shutter_mode;
  i_val = i_val + 8 * (sensor_.trigger_mode);
  sendDataToRegister(handle, 0, i_val); // Register 0

  // Send pixel clock frequency
  switch (sensor_.pixel_clock_frequency)
  {
    case F12_5:
      i_val = 12;
      d_Tlines = sensor_.sensor_line_time[0];
      d_RefClk = 12.5;
      break;
    case F16_666:
      i_val = 14;
      d_Tlines = sensor_.sensor_line_time[1];
      d_RefClk = 16.666;
      break;
    case F20:
      i_val = 15;
      d_Tlines = sensor_.sensor_line_time[2];
      d_RefClk = 20;
      break;
    case F25:
      i_val = 8;
      d_Tlines = sensor_.sensor_line_time[3];
      d_RefClk = 12.5;
      break;
    case F33_333:
      i_val = 10;
      d_Tlines = sensor_.sensor_line_time[4];
      d_RefClk = 16.666;
      break;
    case F40:
      i_val = 11;
      d_Tlines = sensor_.sensor_line_time[5];
      d_RefClk = 20;
      break;
    case F50:
      i_val = 0;
      d_Tlines = sensor_.sensor_line_time[6];
      d_RefClk = 12.5;
      break;
    case F66_666:
      i_val = 2;
      d_Tlines = sensor_.sensor_line_time[7];
      d_RefClk = 16.666;
      break;
    case F80:
      i_val = 3;
      d_Tlines = sensor_.sensor_line_time[8];
      d_RefClk = 20.0;
      break;
    default: // 25 MHz
      i_val = 8;
      d_Tlines = sensor_.sensor_line_time[3];
      d_RefClk = 12.5;
  }
  sendDataToRegister(handle, 1, i_val); // Register 1

  // Send digital and analog gains
  i_val = sensor_.digital_gain;
  i_val = i_val + 64 * (sensor_.analog_gain);
  sendDataToRegister(handle, 2, i_val); // Register 2

  // Send digital offset
  sendDataToRegister(handle, 3, sensor_.digital_offset); // Register 3

  double d_Tblankfactor;
  // Send Exposition
  sendDataToRegister(handle, 4, 49); // Register 4 // FIXME

  // Send resolution
  // 1st colon
  sendDataToRegister(handle, 7, sensor_.first_colon / 4); // Register 7
  // Number of colons
  sendDataToRegister(handle, 8, sensor_.image_width / 8); // Register 8
  // 1st line
  sendDataToRegister(handle, 9, sensor_.first_line / 4); // Register 9
  // Number of lines
  sendDataToRegister(handle, 10, sensor_.image_height / 8); // Register 10

  // Adjust FPS
  double d_TFps, d_Texpo, d_Tread;
  double d_LinesBlank, d_Tblank, i_Tblank;
  int i_LinesRead, i_LinesBlank;

  // Frame period
  d_TFps = 1e6 / (sensor_.frames_per_second);

  if (sensor_.shutter_mode == DIFFERENTIAL)
    d_Texpo = 0;
  else if (sensor_.shutter_mode == GLOBAL)
    d_Texpo = 48;
  else
    d_Texpo = (0 + 1) * 100;

  d_Texpo = 100;
  d_Tread = d_TFps - d_Texpo;

  i_LinesRead = (sensor_.first_line) + 1;
  d_LinesBlank = (d_Tread / d_Tlines) - i_LinesRead;
  i_LinesBlank = floor(d_LinesBlank / 4) * 4;
  d_Tblank = d_TFps - ((i_LinesRead + i_LinesBlank) * d_Tlines + d_Texpo);

  if ((sensor_.sensor_number == 803) || (sensor_.sensor_number == 806) || (sensor_.sensor_number == 902)
      || (sensor_.sensor_number == 1001) || (sensor_.sensor_number == 1101) || (sensor_.sensor_number == 1104)
      || (sensor_.sensor_number == 1201))
    d_Tblankfactor = 16;
  else
    d_Tblankfactor = 32;

  i_Tblank = floor(d_Tblank * d_RefClk / d_Tblankfactor);

  i_val = i_LinesBlank / 4;
  i_val = 11; // FIXME Hard-coded value !!!
  sendDataToRegister(handle, 5, i_val);

  i_val = i_Tblank;
  i_val = 18; // FIXME Hard-coded value !!!
  sendDataToRegister(handle, 6, i_val);

  // Noise compensation
  sendDataToRegister(handle, 12, sensor_.fpn_correction);
  return 0;
}

void NITCameraHandler::captureFrame(const ros::TimerEvent&)
{
#ifdef CHRONO_DEBUG
  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
#endif
  int rvalue, transferred(0);
  {
    std::lock_guard<std::mutex> guard(capture_lock_);

    // Necessary with color
    sendUSBCommand(handle_, VR_SetEP6L, 0);
    sendUSBCommand(handle_, VR_SetEP6H, 0);
#ifdef CHRONO_DEBUG_USB_COMMAND
    std::chrono::steady_clock::time_point end_0 = std::chrono::steady_clock::now();
    unsigned usb_cmb_duration(std::chrono::duration_cast<std::chrono::milliseconds>(end_0 - begin).count());
    ROS_INFO_STREAM("USB command = " << usb_cmb_duration << " milliseconds)");
#endif

    rvalue = cyusb_bulk_transfer(handle_, endpoint_, buffer_, packet_length_, &transferred,
                                 sensor_.capture_timeout_ms);
  }

#ifdef CHRONO_DEBUG
  std::chrono::steady_clock::time_point end_1 = std::chrono::steady_clock::now();
  unsigned capture_duration(std::chrono::duration_cast<std::chrono::milliseconds>(end_1 - begin).count());
  double framerate(1 / (capture_duration / 1e3));
  ROS_INFO_STREAM("Frame rate = " << framerate << " (" << capture_duration << " milliseconds)");
#endif

  if (rvalue != 0 ||
      (unsigned)transferred != sensor_.image_width * sensor_.image_height * 2)
  {
    //ROS_WARN_STREAM("Could not fetch image! Return value = " << rvalue << std::endl <<
    //                "Transfered = " << (unsigned)transferred);
    return;
  }
  /*
   else
   {
   ROS_INFO_STREAM("Image fetched! Return value = " << rvalue << std::endl <<
   "Transfered = " << (unsigned)transferred);
   }*/

  cv::Mat frame = cv::Mat::zeros(sensor_.image_height, sensor_.image_width, CV_32F);
  cv::Mat frame_16 = cv::Mat::zeros(sensor_.image_height, sensor_.image_width, CV_16U);
  frame_16.data = buffer_;

  ushort* Mj = frame_16.ptr<ushort>(0);
  float* Mfj = frame.ptr<float>(0);

  {
    std::lock_guard<std::mutex> guard(hist_AGC_lock_);
    for (auto &agc : hist_AGC_)
      agc = 0;

    for (unsigned i(0); i < sensor_.image_width * sensor_.image_height; ++i)
    {
      Mfj[i] = cv::saturate_cast<float>(Mj[i]);
      hist_AGC_[Mj[i]] = hist_AGC_[Mj[i]] + 1;
    }
    AGCControlProcessing(hist_AGC_, frame);
  }

  {
    std::lock_guard<std::mutex> guard(frame_lock_);
    if (sensor_.has_bayer_filter)
    {
      cv::cvtColor(frame, frame_, sensor_.bayer_OpenCV_code); // color image
      if (publisher_)
        publisher_->publish(cv_bridge::CvImage(std_msgs::Header(), "bgr8", frame_).toImageMsg());
    }
    else
    {
      frame_ = frame;  // monochrome image
      if (publisher_)
        publisher_->publish(cv_bridge::CvImage(std_msgs::Header(), "mono8", frame_).toImageMsg());
    }

    ROS_INFO_STREAM("New frame!");
#ifdef CHRONO_DEBUG
    std::chrono::steady_clock::time_point end_2 = std::chrono::steady_clock::now();
    unsigned callback_duration(std::chrono::duration_cast<std::chrono::milliseconds>(end_2 - begin).count());
    ROS_INFO_STREAM("Callback duration = " << callback_duration << " milliseconds");
#endif
  }
}

unsigned NITCameraHandler::getFrameRate()
{
  return sensor_.frames_per_second;
}

cv::Mat NITCameraHandler::getFrame()
{
  std::lock_guard<std::mutex> guard(frame_lock_);
  return frame_;
}

// Private
int NITCameraHandler::sendUSBCommand(cyusb_handle* handle,
                                     const int n_request,
                                     const int data)
{
  uchar buf[1];
  int transferred(cyusb_control_transfer(handle, 0x40, n_request, data, 0x00, buf, 0, 0));
  return transferred;
}

int NITCameraHandler::sendDataToRegister(cyusb_handle* handle,
                                         const int reg_addr,
                                         const int reg_val)
{
  ushort data;
  data = reg_addr * 256 + reg_val;
  int transferred = sendUSBCommand(handle, NIT_CAMERA_VR_IO_Control, data);
  return transferred;
}

void NITCameraHandler::AGCControlProcessing(const std::vector<short> &HistAGC,
                                            cv::Mat &frame)
{
  cv::Mat frame_tmp(frame);
  double min_val, max_val;

  int i = 0;
  min_val = (double)i;
  int accu = HistAGC[i];

  while (accu < 200)
  {
    i++;
    min_val = (double)i;
    accu += HistAGC[i];
  }

  i = total_max_;
  max_val = (double)i;
  accu = HistAGC[i];

  while (accu < 200)
  {
    i--;
    max_val = (double)i;
    accu = accu + HistAGC[i];
  }

  frame_tmp = (255.0 * (frame - (float)min_val) / ((float)max_val - (float)min_val));
  frame_tmp.convertTo(frame, CV_8U);
}

// Helper to get sensor definitions / parameters
NITCameraHandler::SensorDefinitionParameters NITCameraHandlerSensorDefinitionParameters(const std::string sensor_name)
{
  NITCameraHandler::SensorDefinitionParameters sensor;

  if (sensor_name == "NSC1003")
  {
    sensor.sensor_name = "NSC1003";
    sensor.sensor_number = 1003;
    sensor.shutter_mode = NITCameraHandler::GLOBAL;
    sensor.trigger_mode = NITCameraHandler::NO_EXTERNAL_SIGNAL;
    sensor.sensor_width = 1288;
    sensor.sensor_height = 1032;
    sensor.image_width = 1280;
    sensor.image_height = 1024;
    sensor.first_colon = 4;
    sensor.first_line = 4;
    sensor.sensor_line_time[0] = 1308 / 12.5;
    sensor.sensor_line_time[1] = 1308 / 16.666;
    sensor.sensor_line_time[2] = 1308 / 20.0;
    sensor.sensor_line_time[3] = 1308 / 25.0;
    sensor.sensor_line_time[4] = 1308 / 33.333;
    sensor.sensor_line_time[5] = 1308 / 40.0;
    sensor.sensor_line_time[6] = 1308 / 50.0;
    sensor.sensor_line_time[7] = 1308 / 66.666;
    sensor.sensor_line_time[8] = 1308 / 80.0;
    sensor.pixel_depth = 14;
    sensor.has_bayer_filter = true;
    sensor.bayer_OpenCV_code = cv::COLOR_BayerBG2BGR_VNG;
    sensor.capture_timeout_ms = 200;
    sensor.pixel_clock_frequency = NITCameraHandler::F66_666;
    sensor.analog_gain = NITCameraHandler::DECIBELS_0;
    sensor.digital_gain = NITCameraHandler::G1;
    sensor.digital_offset = 0;
    sensor.frames_per_second = 42;
    sensor.fpn_correction = NITCameraHandler::FPN_0_1;
  }
  //else if (sensor_name == "xxxx")
  else
    throw std::runtime_error("Sensor not recognized!");

  return sensor;
}
