#include <iostream>
#include <thread>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <opencv2/opencv.hpp>

#include "nit_camera/nit_camera_handler.hpp"

// FIXME Publish image on a ROS topic
// FIXME Service to configure the camera
// FIXME Service to modify text written on the image
// FIXME Service to start/stop camera
// FIXME Service to start/stop recording

int main(int argc,
         char *argv[])
{
  ros::init(argc, argv, "nit_camera");
  ros::NodeHandle nh;

  // Fetch node parameters
  std::string image_topic_name;
  nh.param<std::string>("image_topic_name", image_topic_name, "nit_camera/image");

  // Set-up image publisher
  std::shared_ptr<ros::Publisher> publisher(new ros::Publisher);
  *publisher = nh.advertise<sensor_msgs::Image>(image_topic_name, 10, false);

  // Initialize and start camera
  std::shared_ptr<NITCameraHandler> camera_handler;
  try
  {
    NITCameraHandler::SensorDefinitionParameters sensor(NITCameraHandlerSensorDefinitionParameters("NSC1003"));
    camera_handler.reset(new NITCameraHandler(sensor));
    camera_handler->publisher_ = publisher;
/*
    while (ros::ok())
      camera_handler->captureFrame();
      */
  }
  catch (std::exception &error)
  {
    ROS_ERROR_STREAM(error.what());
    return 1;
  }

  ros::spin();
  ros::waitForShutdown();
  return 0;
}
